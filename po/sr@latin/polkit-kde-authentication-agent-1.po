# Translation of polkit-kde-authentication-agent-1.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2010, 2011, 2012, 2015.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: polkit-kde-authentication-agent-1\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-31 01:57+0000\n"
"PO-Revision-Date: 2015-01-17 13:25+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Časlav Ilić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "caslav.ilic@gmx.net"

#: IdentitiesModel.cpp:27
#, kde-format
msgid "Select User"
msgstr "Izbor korisnika"

#: IdentitiesModel.cpp:45
#, kde-format
msgctxt "%1 is the full user name, %2 is the user login name"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: main.cpp:48
#, kde-format
msgid "(c) 2009 Red Hat, Inc."
msgstr "© 2009, RedHat"

#: main.cpp:49
#, kde-format
msgid "Lukáš Tinkl"
msgstr "Lukaš Tinkl"

#: main.cpp:49
#, kde-format
msgid "Maintainer"
msgstr "Održavalac"

#: main.cpp:50
#, kde-format
msgid "Jaroslav Reznik"
msgstr "Jaroslav Reznik"

#: main.cpp:50
#, kde-format
msgid "Former maintainer"
msgstr "Bivši održavalac"

#: policykitlistener.cpp:71
#, kde-format
msgid "Another client is already authenticating, please try again later."
msgstr "Trenutno se autentifikuje drugi klijent, pokušajte ponovo kasnije."

#~ msgid "Action:"
#~ msgstr "Radnja:"

#~ msgid "Vendor:"
#~ msgstr "Izdavač:"

#, fuzzy
#~| msgid "Action:"
#~ msgid "Action ID:"
#~ msgstr "Radnja:"

#~ msgid "Details"
#~ msgstr "Detalji"

#~ msgid "Authentication Required"
#~ msgstr "Neophodna autentifikacija"

#~ msgid "Password for root:"
#~ msgstr "Lozinka korena:"

# rewrite-msgid: /for/for account/
#~ msgid "Password for %1:"
#~ msgstr "Lozinka naloga %1:"

#~ msgid "Password:"
#~ msgstr "Lozinka:"

#~ msgid "Password or swipe finger for root:"
#~ msgstr "Lozinka ili otisak prsta korena:"

# rewrite-msgid: /for/for account/
#~ msgid "Password or swipe finger for %1:"
#~ msgstr "Lozinka ili otisak prsta naloga %1:"

#~ msgid "Password or swipe finger:"
#~ msgstr "Lozinka ili otisak prsta:"

#~ msgid ""
#~ "An application is attempting to perform an action that requires "
#~ "privileges. Authentication is required to perform this action."
#~ msgstr ""
#~ "Program pokušava da izvede radnju koja zahteva ovlašćenja. Zato je "
#~ "neophodna autentifikacija."

#~ msgid "Authentication failure, please try again."
#~ msgstr "Autentifikacija nije uspela, pokušajte ponovo."

#~ msgctxt ""
#~ "%1 is the name of a detail about the current action provided by polkit"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "Click to open %1"
#~ msgstr "Kliknite da otvorite %1"

#~ msgid "P&assword:"
#~ msgstr "&Lozinka:"

# rewrite-msgid: /Agent//
#~ msgid "PolicyKit1 KDE Agent"
#~ msgstr "PolicyKit1‑KDE"
